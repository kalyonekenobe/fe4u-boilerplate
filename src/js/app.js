const axios = require('axios').default;
const lodash = require('lodash');
const {
  uniquify,
  generateTeachersListHtml,
  convertToTeacherList,
  convertToTeacher,
  showAddTeacherPopup,
  generateFavoriteTeachersListHtml,
  filterTeacherList,
  searchTeachersByQuery,
  generateCountriesSelectHtml,
  addClickEventListenersForTeacherCards,
  request,
  validateTeacherObject,
  generateFormErrorHtml,
  setupPieCharts
} = require('./functions');
require('../css/index.css');

request("https://randomuser.me/api/?results=50").then(response => {
  let teachersList = uniquify(convertToTeacherList(response.results));

  let loadMoreButton = document.querySelector('.pagination a');

  const countries = new Set(_.filter(teachersList, teacher => {
    return teacher.country !== undefined;
  }).map(user => user.country));

  generateCountriesSelectHtml(countries);

  let filteredTeachersList = structuredClone(teachersList);
  let statisticsTableColumns = document.querySelectorAll('.statistics-table th');

  let statisticsPaginationData = {
    teachersList: filteredTeachersList,
    sortedStatisticsTableField: "full_name",
    currentStatisticsTablePage: 1,
    teachersPerPage: 10
  };

  loadMoreButton.addEventListener('click', () => {
    request("https://randomuser.me/api/?results=10").then(loadMoreResponse => {
      teachersList = [...teachersList, ...uniquify(convertToTeacherList(loadMoreResponse.results))]
      filteredTeachersList = structuredClone(teachersList);
      statisticsPaginationData.teachersList = filteredTeachersList;
      generateTeachersListHtml(filteredTeachersList);
      generateFavoriteTeachersListHtml(_.filter(filteredTeachersList, {'favorite': true}))
      addClickEventListenersForTeacherCards(filteredTeachersList);
    })
  });

  generateTeachersListHtml(filteredTeachersList);
  generateFavoriteTeachersListHtml(_.filter(filteredTeachersList, {'favorite': true}))
  addClickEventListenersForTeacherCards(filteredTeachersList);
  setupPieCharts(teachersList);

  let filtersForm = document.querySelector('.filters-form');
  let filters = []

  filtersForm.addEventListener('change', event => {
    let fieldMinValue, fieldMaxValue;
    switch (event.target.dataset.field) {
      case "age":
        fieldMinValue = Number(event.target.value.split('-')[0]);
        fieldMaxValue = Number(event.target.value.split('-')[1]);
        break;
      case "country":
        if (event.target.value && event.target.value !== '') {
          fieldMinValue = event.target.value;
          fieldMaxValue = event.target.value;
        }
        break;
      case "gender":
        if (event.target.value && event.target.value !== '') {
          fieldMinValue = event.target.value;
          fieldMaxValue = event.target.value;
        }
        break;
      case "picture_thumbnail":
        if (event.target.checked)
          fieldMinValue = '';
        break;
      case "favorite":
        if (event.target.checked) {
          fieldMinValue = 1;
          fieldMaxValue = 1;
        }
        break;
    }

    filters = _.filter(filters, f => f.name !== event.target.dataset.field);
    if (fieldMinValue && fieldMaxValue) {
      filters.push({
        name: event.target.dataset.field,
        minValue: fieldMinValue,
        maxValue: fieldMaxValue
      });
    }

    filteredTeachersList = structuredClone(teachersList);
    filters.forEach(filter => {
      filteredTeachersList = filterTeacherList(filteredTeachersList, filter);
    });

    statisticsPaginationData.teachersList = filteredTeachersList;
    generateTeachersListHtml(filteredTeachersList);
    generateFavoriteTeachersListHtml(_.filter(filteredTeachersList, {'favorite': true}))
    addClickEventListenersForTeacherCards(filteredTeachersList);
  })

  let searchForm = document.querySelector('.search-form');

  searchForm.addEventListener('submit', event => {
    event.preventDefault();
    let value = event.target.querySelector('input[name="search-text"]').value;
    let searchTeachersList = searchTeachersByQuery(filteredTeachersList, value);
    statisticsPaginationData.teachersList = searchTeachersList;
    generateTeachersListHtml(searchTeachersList);
    generateFavoriteTeachersListHtml(_.filter(searchTeachersList, {'favorite': true}));
    addClickEventListenersForTeacherCards(searchTeachersList);
  })

  let addTeacherButtons = document.querySelectorAll('.add-teacher-button');
  addTeacherButtons.forEach(button => button.addEventListener('click', () => {
    showAddTeacherPopup();
  }));

  let addTeacherForm = document.querySelector('.add-teacher-form');

  addTeacherForm.addEventListener('change', () => {
    addTeacherForm.querySelectorAll('.form-error').forEach(error => error.remove());
  });

  addTeacherForm.addEventListener('submit', event => {
    event.preventDefault();
    let inputs = Object.fromEntries(new FormData(event.target));
    let lifetime = new Date(new Date().getTime() - new Date(inputs.teacher_date_of_birth).getTime());
    let newTeacher = convertToTeacher({
      full_name: inputs.teacher_name,
      course: inputs.teacher_speciality,
      country: inputs.teacher_country,
      city: inputs.teacher_city,
      email: inputs.teacher_email,
      phone: inputs.teacher_phone,
      b_date: inputs.teacher_date_of_birth,
      gender: inputs.teacher_sex,
      bg_color: inputs.background_color,
      note: inputs.notes,
      state: inputs.teacher_city,
      age: Math.abs(lifetime.getUTCFullYear() - 1970),
      title: inputs.teacher_sex === 'Male' ? 'Mr' : 'Mrs',
    })

    if (!validateTeacherObject(newTeacher)) {
      if (typeof newTeacher.full_name !== 'string' ||
        newTeacher.full_name[0].toUpperCase() !== newTeacher.full_name[0]) {
        let container = addTeacherForm.querySelector('.teacher-name-form-item');
        console.log(container);
        container.append(
          generateFormErrorHtml("The name of the teacher must begin with a capital letter!")
        );
      }

      if (typeof newTeacher.city !== 'string' ||
        newTeacher.city[0].toUpperCase() !== newTeacher.city[0]) {
        let container = addTeacherForm.querySelector('.teacher-city-form-item');
        console.log(container);
        container.append(
          generateFormErrorHtml("The city of the teacher must begin with a capital letter!")
        );
      }

      const phoneRegex = "(\\+?\d{1,3})?(((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-" +
        "| )?(\\d{2})){2}|((\\-| )?(\\d{3,}))))|(((\\-| )?(\\(\\d{2}\\)|\\d{2}))((\\-| )?\\d{2}){4})";
      if (!newTeacher.phone.match(phoneRegex)) {
        let container = addTeacherForm.querySelector('.teacher-phone-form-item');
        console.log(container);
        container.append(generateFormErrorHtml("Incorrect phone format!"));
      }

      if (!newTeacher.email.match("[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}")) {
        let container = addTeacherForm.querySelector('.teacher-email-form-item');
        console.log(container);
        container.append(generateFormErrorHtml("Incorrect email format!"));
      }
    } else {
      axios.post("http://localhost:3000/teachers", newTeacher);
      addTeacherForm.reset();
      addTeacherForm.querySelectorAll('.form-error').forEach(error => error.remove());
      teachersList = [...teachersList, newTeacher];
      filteredTeachersList = [...filteredTeachersList, newTeacher];
      filters.forEach(filter => {
        filteredTeachersList = filterTeacherList(filteredTeachersList, filter);
      });
      statisticsPaginationData.teachersList = filteredTeachersList;
      generateTeachersListHtml(filteredTeachersList);
      generateFavoriteTeachersListHtml(_.filter(filteredTeachersList, {'favorite': true}));
      addClickEventListenersForTeacherCards(filteredTeachersList);
      let addTeacherPopup = document.querySelector('.add-teacher-popup');
      addTeacherPopup.style.display = 'none';
      document.body.style.overflow = 'auto';
    }
  })
});