const dayjs = require('dayjs');
const _ = require('lodash');

export const setupPieCharts = teachersList => {
  setupPieChart({
    labels: _.map(teachersList, teacher => teacher.full_name),
    data: _.map(teachersList, () => 1),
    title: "Teacher names"
  }, "chart-container-names");
  let specialities = uniquify(structuredClone(_.map(teachersList, teacher => teacher.course)));
  setupPieChart({
    labels: specialities,
    data: _.map(specialities, speciality => teachersList.reduce((count, teacher) => {
      return count + (teacher.course === speciality ? 1 : 0)
    }, 0)),
    title: "Teacher specialities"
  }, "chart-container-specialities");
  let ages = uniquify(structuredClone(_.map(teachersList, teacher => teacher.age)));
  setupPieChart({
    labels: ages,
    data: _.map(ages, age => teachersList.reduce((count, teacher) => {
      return count + (teacher.age === age ? 1 : 0)
    }, 0)),
    title: "Teacher ages"
  }, "chart-container-ages");
  let genders = uniquify(structuredClone(_.map(teachersList, teacher => teacher.gender)));
  setupPieChart({
    labels: genders,
    data: _.map(genders, gender => teachersList.reduce((count, teacher) => {
      return count + (teacher.gender === gender ? 1 : 0)
    }, 0)),
    title: "Teacher genders"
  }, "chart-container-genders");
  let nationalities = uniquify(structuredClone(_.map(teachersList, teacher => teacher.country)));
  setupPieChart({
    labels: nationalities,
    data: _.map(nationalities, nationality => teachersList.reduce((count, teacher) => {
      return count + (teacher.country === nationality ? 1 : 0)
    }, 0)),
    title: "Teacher nationalities"
  }, "chart-container-nationalities");
}

export const setupPieChart = (data, chartId) => {

  const chartData = {
    labels: data.labels,
    datasets: [{
      label: 'Count',
      data: data.data,
      backgroundColor: data.data.map(() => `#${getRandomInRange(0, 16 ** 6 - 1).toString(16)}`),
      hoverOffset: 4
    }]
  };
  
  const config = {
    type: 'pie',
    data: chartData,
    options: {
      responsive: false,
      plugins: {
        title: {
          display: true,
          text: data.title,
          font: {
            size: 20
          }
        },
        legend: {
          display: false
        }
      }
    }
  };

  let chart = document.querySelector(`#${chartId}`);
  new Chart(chart, config);
  chart.style.width = "300px";
  chart.style.height = "300px";
  chart.style.marginTop = "50px";
  chart.style.marginBottom = "50px";
}

export const generateFormErrorHtml = message => {
  let errorContainer = document.createElement("div");
  errorContainer.classList.add("form-error");
  errorContainer.innerHTML = message;
  return errorContainer;
}

export const generateStatisticsPaginationHtml = statisticsPaginationData => {
  let statisticsPaginationHtml = '';
  let teachersNumber = statisticsPaginationData.teachersList.length;
  let limit = statisticsPaginationData.teachersPerPage;
  let numberOfPages = Math.floor((teachersNumber + limit - 1) / limit);
  let currentActivePage = statisticsPaginationData.currentStatisticsTablePage;
  statisticsPaginationHtml += `
    <a ${currentActivePage == 1 ? `class="active"` : ``} data-page="1">1</a>
  `;
  if (Math.min(currentActivePage, Math.ceil(numberOfPages / 2)) - 3 > 1 && numberOfPages > 7) {
    statisticsPaginationHtml += `
      <a class="dots">...</a>
    `;
  }
  let from = Math.max(2, Math.min(currentActivePage, Math.ceil(numberOfPages / 2)) - 2);
  let to = Math.min(Math.max(currentActivePage, Math.ceil(numberOfPages / 2)) + 2, numberOfPages - 1);
  for (let i = from; i <= to; i++) {
    statisticsPaginationHtml += `
      <a ${currentActivePage == i ? `class="active"` : ``} data-page="${i}">${i}</a>
    `;
  }
  if (Math.max(currentActivePage, Math.ceil(numberOfPages / 2)) + 3 < numberOfPages && numberOfPages > 7) {
    statisticsPaginationHtml += `
      <a class="dots">...</a>
    `;
  }
  statisticsPaginationHtml += `
    <a ${currentActivePage == numberOfPages ?
      `class="active"`
      :
      ``
    } data-page="${numberOfPages}">${numberOfPages}</a>
  `;
  let statisticsPagination = document.querySelector('.statistics-pagination');
  statisticsPagination.innerHTML = statisticsPaginationHtml;
  let statisticsTablePages = document.querySelectorAll('.statistics-pagination a:not(.dots)');
  statisticsTablePages.forEach(page => page.addEventListener('click', () => {
    let pageNumber = page.dataset.page;
    statisticsTablePages.forEach(p => p.classList.remove('active'));
    page.classList.add('active');
    generateStatisticsTableHtml(
      sortTeacherListByField(
        statisticsPaginationData.teachersList,
        statisticsPaginationData.sortedStatisticsTableField
      ),
      pageNumber,
      statisticsPaginationData.teachersPerPage
    );
    statisticsPaginationData.currentStatisticsTablePage = pageNumber;
    generateStatisticsPaginationHtml(statisticsPaginationData);
  }));
}

export const request = (url = ``, method = "GET", data = {}) => {
  const requestParams = {
    method: method,
    cache: "no-cache",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
  }
  if (method !== "GET")
    requestParams = { ...requestParams, body: JSON.stringify(data) };
  return fetch(url, requestParams).then(response => response.json());
}

export const generateStatisticsTableHtml = (teachersList, page, limit) => {
  let statisticsTableBodyHtml = '';
  for (let i = (page - 1) * limit; i < Math.min(teachersList.length, page * limit); i++) {
    let teacher = teachersList[i];
    statisticsTableBodyHtml += `
      <tr>
        <td>${teacher.full_name}</td>
        <td>${teacher.course}</td>
        <td>${teacher.age}</td>
        <td>${teacher.gender}</td>
        <td>${teacher.country}</td>
      </tr>
    `;
  }
  let statisticsTableBody = document.querySelector('.statistics-table tbody');
  statisticsTableBody.innerHTML = statisticsTableBodyHtml;
}

export const showTeacherInfoPopup = (teacher, teachersList) => {
  if (!teacher)
    return;
  let teacherInfoPopup = document.getElementById("teacher-info-popup");
  teacherInfoPopup.style.display = "flex";
  document.body.style.overflow = "hidden";
  teacherInfoPopup.innerHTML = generateTeacherInfoPopupHtml(teacher);
  let closeButton = teacherInfoPopup.querySelector(".close");
  let starButton = teacherInfoPopup.querySelector('.fa-star');
  let toggleMapButton = teacherInfoPopup.querySelector('.toggle-map');
  starButton.addEventListener('click', () => {
    teacher.favorite = !teacher.favorite;
    teachersList = _.map(teachersList, t => t.id === teacher.id ? teacher : t);
    generateTeachersListHtml(teachersList);
    generateFavoriteTeachersListHtml(_.filter(teachersList, {'favorite': false}))
    addClickEventListenersForTeacherCards(teachersList);
    showTeacherInfoPopup(teacher, teachersList);
  });
  toggleMapButton.addEventListener('click', () => {
    let mapContainer = teacherInfoPopup.querySelector('#teacher-map');
    if (window.getComputedStyle(mapContainer).display === "none") {
      setTimeout(() => {
        let map = L.map('teacher-map').setView(
          [teacher.coordinates.latitude, teacher.coordinates.longitude], 
          3
        );
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 19,
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
        L.marker([teacher.coordinates.latitude, teacher.coordinates.longitude]).addTo(map);
        closeButton.addEventListener('click', () => {
          teacherInfoPopup.style.display = "none";
          document.body.style.overflow = 'auto';
        });
      }, 500)
      mapContainer.style.display = "block";
    } else 
      mapContainer.style.display = "none";
  });
}

export const showAddTeacherPopup = () => {
  let addTeacherPopup = document.querySelector('.add-teacher-popup');
  addTeacherPopup.style.display = 'flex';
  document.body.style.overflow = 'hidden';
  generateCoursesSelectHtml(courses);
  let closeButton = addTeacherPopup.querySelector('.close');
  closeButton.addEventListener('click', () => {
    addTeacherPopup.querySelector('.add-teacher-form').reset();
    addTeacherPopup.querySelectorAll('.add-teacher-form .form-error').forEach(error => error.remove());
    addTeacherPopup.style.display = 'none';
    document.body.style.overflow = 'auto';
  })
}

export const generateTeacherInfoPopupHtml = teacher => {
  if (!teacher)
    return '';
  let daysDifference = dayjs(teacher.b_date).set('year', dayjs().year()).diff(dayjs(), 'days');
  if (daysDifference <= 0)
    daysDifference += 365;
  let teacherInfoPopupHtml = `
    <div class="popup-blur"></div>
    <div class="popup-modal teacher-info-modal">
      <header>
        <h2>Teacher info</h2>
        <a>
          <i class="fa-solid fa-xmark close"></i>
        </a>
      </header>
      <div class="content">
        <div class="teacher-details">
          <div class="image-wrapper">
            <img src="${teacher.picture_large ? teacher.picture_large : ''}" 
              alt="Зображення викладача">
          </div>
          <div class="general-info">
            <div class="teacher-name-container">
              <h3>${teacher.full_name}</h3>
              <i class="${!teacher.favorite ? 'fa-regular' : 'fa-solid'} fa-star"></i>
            </div>
            <h4 class="subject">${teacher.course}</h4>
            <p class="location">${teacher.city}, ${teacher.country}</p>
            <p class="age-and-sex">${teacher.age}, ${teacher.gender}</p>
            <p class="days-to-next-birthday">Days to next birthday: ${daysDifference}</p>
            <p class="email">${teacher.email}</p>
            <p class="phone">${teacher.phone}</p>
          </div>
        </div>
        <div class="teacher-description">
          <p>
            ${teacher.note}
          </p>
        </div>
        <div class="map">
          <a class="link toggle-map">toggle map</a>
          <div id="teacher-map">
          </div>
        </div>
      </div>
    </div>
  `;

  return teacherInfoPopupHtml;
}

export const generateTeachersListHtml = teachersList => {
  let teachersListContainer = document.querySelector('.teachers-list');
  let teacherListHtml = '';
  teachersList.forEach(teacher => teacherListHtml += createTeacherCardHtml(teacher));
  teachersListContainer.innerHTML = teacherListHtml;
}

export const generateFavoriteTeachersListHtml = teachersList => {
  let teachersListContainer = document.querySelector('.favorites-section .teachers-list');
  let teacherListHtml = '';
  teachersList.forEach(teacher => teacherListHtml += createTeacherCardHtml(teacher));
  teachersListContainer.innerHTML = teacherListHtml;
}

export const createTeacherCardHtml = teacher => {
  if (!teacher)
    return '';
  let teacherSplittedFullName = teacher.full_name.split(' ');
  let teacherCardHtml = `
    <div class="teacher-card ${teacher.favorite ? 'stared' : ''}" data-teacher-id='${teacher.id}'>
      <div class="card-header">
        ${teacher.favorite ?
      `<img src="images/star.png" class="star" alt="Відзнака">`
      :
      ``
    }
        ${teacher.picture_large ?
      `
              <div class="image-wrapper">
                <a>
                  <img src="${teacher.picture_large}" alt="Зображення викладача">
                </a>
              </div>
            `
      :
      `
              <div class="image-wrapper no-image">
                <a>
                  <h3>${teacherSplittedFullName[0][0]}.${teacherSplittedFullName[teacherSplittedFullName.length - 1][0]
      }.</h3>
                </a>
              </div>
            `
    }
      </div>
      <h3>${teacher.full_name}</h3>
      <span class="subject">${teacher.course}</span>
      <span class="country">${teacher.country}</span>
    </div>
  `;

  return teacherCardHtml;
}

export const addClickEventListenersForTeacherCards = teachersList => {
  let teacherCards = document.querySelectorAll('.teacher-card');
  teacherCards.forEach(teacherCard => teacherCard.addEventListener('click', () => {
    let teacher = findTeacherInList(teachersList, { id: teacherCard.dataset.teacherId });
    showTeacherInfoPopup(teacher, teachersList);
  }))
}

export const convertToTeacher = object => ({
  "gender": capitalize(object.gender),
  "title": object.title ? object.title : (object.name ? object.name.title : null),
  "full_name": object.full_name ? object.full_name :
    (object.name ? `${capitalize(object.name.first)} ${capitalize(object.name.last)}` : null),
  "city": object.city ? object.city : (object.location ? object.location.city : null),
  "state": object.state ? object.state : (object.location ? object.location.state : null),
  "country": object.country ? object.country : (object.location ? object.location.country : null),
  "postcode": object.postcode ? object.postcode :
    (object.location ? object.location.postcode : null),
  "coordinates": object.coordinates ? object.coordinates :
    (object.location ? object.location.coordinates : null),
  "timezone": object.timezone ? object.timezone :
    (object.location ? object.location.timezone : null),
  "email": object.email,
  "b_date": object.b_date ? object.b_date : (object.dob ? object.dob.date : null),
  "age": object.age ? object.age :
    (object.dob ? object.dob.age : null),
  "phone": object.phone,
  "picture_large": object.picture_large ? object.picture_large :
    (object.picture ? object.picture.large : null),
  "picture_thumbnail": object.picture_thumbnail ? object.picture_thumbnail :
    (object.picture ? object.picture.thumbnail : null),
  "id": crypto.randomUUID(),
  "favorite": object.favorite ? object.favorite : getRandomInRange(false, true) == 1,
  "course": object.course ? object.course : courses[getRandomInRange(0, courses.length - 1)],
  "bg_color": object.bg_color ? object.bg_color :
    `#${getRandomInRange(0, 16 ** 6 - 1).toString(16)}`,
  "note": object.note ? capitalize(object.note) : capitalize(generateRandomNote())
})

export const convertToTeacherList = teachersList => {
  return _.map(teachersList, object => convertToTeacher(object))
}

export const uniquify = teacherList => [...new Set(teacherList)]

export const validateTeacherObject = teacher => {
  if (!_.isString(teacher.full_name) || _.upperFirst(teacher.full_name) !== teacher.full_name)
    return false;

  if (!_.isString(teacher.gender) || _.upperFirst(teacher.gender) !== teacher.gender)
    return false;

  if (!_.isString(teacher.note) || _.upperFirst(teacher.note) !== teacher.note)
    return false;

  if (!_.isString(teacher.state) || _.upperFirst(teacher.state) !== teacher.state)
    return false;

  if (!_.isString(teacher.city) || _.upperFirst(teacher.city) !== teacher.city)
    return false;

  if (!_.isString(teacher.country) || _.upperFirst(teacher.country) !== teacher.country)
    return false;

  if (!_.isNumber(teacher.age))
    return false;

  const phoneRegex = "(\\+?\d{1,3})?(((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-" +
    "| )?(\\d{2})){2}|((\\-| )?(\\d{3,}))))|(((\\-| )?(\\(\\d{2}\\)|\\d{2}))((\\-| )?\\d{2}){4})";
  if (!teacher.phone.match(phoneRegex))
    return false;

  if (!teacher.email.match("[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}"))
    return false;

  return true;
}

export const filterTeacherList = (teacherList, field) => {
  return _.filter(teacherList, teacher => {
    return (field.minValue ? teacher[field.name] >= field.minValue : true) &&
      (field.maxValue ? teacher[field.name] <= field.maxValue : true);
  });
}

export const sortTeacherListByField = (teacherList, field, ascendingOrder = true) => {
  return _.sortBy(teacherList, teacher => ascendingOrder ? teacher[field] : teacher[field]);
}

export const findTeacherInList = (teacherList, params) => {
  return _.find(teacherList, teacher => {
    for (const [key, value] of Object.entries(params)) {
      if (teacher[key] !== value)
        return false;
    }

    return true;
  })
}

export const searchTeachersByQuery = (teacherList, query) => {
  return _.filter(teacherList, teacher => teacher.full_name.includes(query) ||
    teacher.age === Number(query) || teacher.note.includes(query));
}

export const getPercentOfTeachersFilteredByField = (teacherList, field) => {
  const numberOfFilteredTeachers = _.filter(teacherList, teacher => {
    const greaterOrEqualThanMin = field.minValue ? teacher[field.name] >= field.minValue : true;
    const lessOEqualThanMax = field.maxValue ? teacher[field.name] <= field.maxValue : true;

    return greaterOrEqualThanMin && lessOEqualThanMax;
  }).length;

  return numberOfFilteredTeachers / teacherList.length * 100;
}

export const generateRandomNote = () => {
  let numberOfWords = getRandomInRange(1, 250);
  let note = '';

  while (numberOfWords--)
    note += `${words[getRandomInRange(0, words.length - 1)]} `;

  return note;
}

export const getRandomInRange = (min, max) => {
  return Math.floor(Math.random() * (Math.ceil(max) - Math.floor(min) + 1) + Math.floor(min));
}

export const capitalize = string => string ? _.upperFirst(string) : string;

export const generateCountriesSelectHtml = countries => {
  let countriesSelectHtml = '<option value="">Any</option>';
  countries.forEach(country => countriesSelectHtml += `
    <option value="${country}">${country}</option>
  `);
  let teacherCountryFilter = document.querySelector('.filters #country-selector');
  let addTeacherFormCountry = document.querySelector('.add-teacher-popup #country-selector');
  teacherCountryFilter.innerHTML = countriesSelectHtml;
  if (addTeacherFormCountry)
    addTeacherFormCountry.innerHTML = countriesSelectHtml;
}

export const generateCoursesSelectHtml = courses => {
  let coursesSelectHtml = '';
  courses.forEach(course => coursesSelectHtml += `
    <option value="${course}">${course}</option>
  `);
  let addTeacherFormCourseSelect = document.querySelector('.add-teacher-popup #teacher-speciality');
  if (addTeacherFormCourseSelect)
    addTeacherFormCourseSelect.innerHTML = coursesSelectHtml;
}

export const courses = [
  "Mathematics", "Physics", "English",
  "Computer Science", "Dancing", "Chess",
  "Biology", "Chemistry", "Law",
  "Art", "Medicine", "Statistics"
]

export const phoneNumberRegexs = {
  "Germany": "(\\+?49)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Ireland": "(\\+?353)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Australia": "(\\+?61)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "United States": "(\\+?1)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Finland": "(\\+?358)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Turkey": "(\\+?90)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Switzerland": "(\\+?41)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "New Zealand": "(\\+?64)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Spain": "(\\+?34)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Norway": "(\\+?47)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Denmark": "(\\+?45)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Iran": "(\\+?98)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "Canada": "(\\+?1)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
  "France": "(\\+?33)?(((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,}))))|(((\\-| )?(\\(\\d{2}\\)|\\d{2}))((\\-| )?\\d{2}){4})",
  "Netherlands": "(\\+?90)?((\\-| )?(\\(\\d{2,3}\\)|\\d{2,3}))((\\-| )?\\d{3,})(((\\-| )?(\\d{2})){2}|((\\-| )?(\\d{3,})))",
}

export const words = [
  "Lorem", "ipsum", "dolor", "sit", "amet,", "consectetur", "adipiscing", "elit,", "sed", "do",
  "eiusmod", "tempor", "incididunt", "ut", "labore", "et", "dolore", "magna", "aliqua.",
  "mattis", "ullamcorper", "velit.", "Potenti", "nullam", "ac", "tortor", "vitae",
  "purus", "faucibus", "ornare", "suspendisse.", "Integer", "eget", "aliquet", "nibh",
  "praesent", "tristique", "magna.", "Egestas", "maecenas", "pharetra", "convallis", "posuere",
  "morbi", "leo", "urna.", "Velit", "dignissim", "sodales", "ut", "eu.",
  "Vel", "risus", "commodo", "viverra", "maecenas", "accumsan.", "Ipsum", "dolor",
  "sit", "amet", "consectetur", "adipiscing.", "Eget", "est", "lorem", "ipsum",
  "dolor", "sit", "amet", "consectetur", "adipiscing", "elit.", "Dolor", "purus",
  "non", "enim", "praesent", "elementum", "facilisis.", "Pellentesque", "dignissim", "enim",
  "sit", "amet", "venenatis", "urna", "cursus", "eget", "nunc.", "Quis",
  "risus", "sed", "vulputate", "odio", "ut", "enim", "blandit", "volutpat",
  "maecenas.", "Eget", "dolor", "morbi", "non", "arcu", "risus.", "Libero",
  "enim", "sed", "faucibus", "turpis", "in.", "Eget", "duis", "at",
  "tellus", "at", "urna", "condimentum", "mattis", "pellentesque", "id.", "A",
  "cras", "semper", "auctor", "neque", "vitae", "tempus", "quam", "pellentesque",
  "nec.", "Arcu", "odio", "ut", "sem", "nulla", "pharetra", "diam",
  "sit.", "Amet", "cursus", "sit", "amet", "dictum", "sit", "amet",
  "justo", "donec", "enim.", "Urna", "neque", "viverra", "justo", "nec.",
  "Maecenas", "volutpat", "blandit", "aliquam", "etiam", "erat.", "Porttitor", "lacus",
  "luctus", "accumsan", "tortor.", "Felis", "donec", "et", "odio", "pellentesque",
  "diam", "volutpat", "commodo", "sed.", "Mattis", "vulputate", "enim", "nulla",
  "aliquet", "porttitor.", "Feugiat", "vivamus", "at", "augue", "eget", "arcu",
  "dictum.", "Nunc", "mattis", "enim", "ut", "tellus", "elementum", "sagittis",
  "vitae.", "Volutpat", "diam", "ut", "venenatis", "tellus", "in", "metus",
  "vulputate", "eu.", "Eget", "sit", "amet", "tellus", "cras", "adipiscing",
  "enim", "eu.", "Morbi", "enim", "nunc", "faucibus", "a", "pellentesque",
  "sit.", "Sed", "risus", "ultricies", "tristique", "nulla", "aliquet", "enim",
  "tortor", "at", "auctor.", "Augue", "eget", "arcu", "dictum", "varius",
  "duis.", "At", "volutpat", "diam", "ut", "venenatis", "tellus", "in",
  "metus", "vulputate.", "Amet", "mauris", "commodo", "quis", "imperdiet", "massa",
  "tincidunt", "nunc", "pulvinar.", "Netus", "et", "malesuada", "fames", "ac.",
]
